from django.shortcuts import render, redirect, get_object_or_404
import braintree
from orders.models import Order

def payment_process(request):
    order_id = request.session.get('order_id')
    order = get_object_or_404(Order, id=order_id)

    if request.methid == 'POST':
        nonce = request.POST
